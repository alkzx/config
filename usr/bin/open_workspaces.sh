
wait_for_program () {
        n=0
        while true
        do
            # PID of last background command
            if xdotool search --onlyvisible --pid $!; then
                break
            else
                # 2 seconds timeout
                if [ $n -eq 20 ]; then
                    xmessage "Error on executing"
                    break
                else
                    n=`expr $n + 1`
                    sleep 0.1
                fi
            fi
        done
}

sleep 1

i3-msg workspace "1: work"
urxvt256c &
wait_for_program

i3-msg workspace "2: docs1"
urxvt256c &
wait_for_program

i3-msg workspace "3: virtual"
urxvt256c &
wait_for_program

i3-msg workspace "4: stuff1"
urxvt256c &
wait_for_program

i3-msg workspace "5: docs2"
urxvt256c &
wait_for_program

i3-msg workspace "6: stuff2"
urxvt256c &
wait_for_program

i3-msg workspace "7: social1"
urxvt256c &
wait_for_program

i3-msg workspace "8: social2"
urxvt256c &
wait_for_program

i3-msg workspace "9: stuff3"
urxvt256c &
wait_for_program

i3-msg workspace "10: tests"
urxvt256c &
wait_for_program
