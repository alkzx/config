hi clear

if exists("syntax on")
	syntax reset
endif

let g:colors_name = "frozenblood"

" make stuff more readable
set fcs=vert:│,fold:\ 
set fillchars+=stl:\ ,stlnc:\ 
set listchars=tab:·\ ,trail:░,extends:»,precedes:«
set list

"if &term =~ "rxvt"
"  silent !echo -ne "\033]12;\#404040\007"
"  let &t_SI = "\033]12;\#b21818\007"
"  let &t_EI = "\033]12;\#404040\007"
"  autocmd VimLeave * :silent !echo -ne "\033]12;\#b21818\007"

"endif

set t_Co=256 " Explicitly tell vim that the terminal supports 256 colors

" General colors
hi Normal        cterm=none      ctermfg=251      ctermbg=none
hi Directory     cterm=none      ctermfg=blue       ctermbg=none
hi ErrorMsg      cterm=none      ctermfg=blue   ctermbg=none
hi NonText       cterm=none      ctermfg=darkgray  ctermbg=none
hi SpecialKey    cterm=none      ctermfg=236       ctermbg=none
hi LineNr        cterm=none      ctermfg=darkgrey  ctermbg=none
hi IncSearch     cterm=none      ctermfg=black     ctermbg=blue
hi Search        cterm=none      ctermfg=black     ctermbg=blue
hi Visual        cterm=none      ctermfg=black     ctermbg=blue
hi VisualNOS     cterm=none      ctermfg=black     ctermbg=blue
hi MoreMsg       cterm=none      ctermfg=darkgreen ctermbg=none
hi ModeMsg       cterm=bold      ctermfg=none      ctermbg=none
hi Question      cterm=none      ctermfg=darkgreen ctermbg=none
hi WarningMsg    cterm=none      ctermfg=blue   ctermbg=none
hi WildMenu      cterm=none      ctermfg=white     ctermbg=none
hi TabLine       cterm=underline ctermfg=white     ctermbg=none
hi TabLineSel    cterm=underline ctermfg=white     ctermbg=24
hi TabLineFill   cterm=underline ctermfg=white     ctermbg=none
hi DiffAdd       cterm=none      ctermfg=white     ctermbg=darkgreen
hi DiffChange    cterm=underline ctermfg=none      ctermbg=none
hi DiffDelete    cterm=none      ctermfg=white     ctermbg=blue
hi DiffText      cterm=none      ctermfg=white     ctermbg=none
hi SignColumn    cterm=none      ctermfg=blue   ctermbg=none
hi StatusLine    cterm=underline ctermfg=black     ctermbg=blue
hi StatusLineNC  cterm=underline ctermfg=white     ctermbg=none
hi VertSplit     cterm=none      ctermfg=blue   ctermbg=none
hi CursorColumn  cterm=none      ctermfg=none      ctermbg=blue 
hi CursorLineNr  cterm=none      ctermfg=none      ctermbg=blue
hi CursorLine    cterm=none      ctermfg=none      ctermbg=blue
hi ColorColumn   cterm=none      ctermfg=none      ctermbg=blue
hi Cursor        cterm=none      ctermfg=white     ctermbg=blue guibg=blue
hi Title         cterm=bold      ctermfg=white     ctermbg=none
hi Pmenu         cterm=none      ctermfg=blue   ctermbg=none
hi PmenuSel      cterm=none      ctermfg=black     ctermbg=blue
hi PmenuSbar     cterm=none      ctermfg=white     ctermbg=blue
hi Folded        cterm=none      ctermfg=blue   ctermbg=none
hi FoldColumn    cterm=none      ctermfg=blue   ctermbg=none
hi MatchParen    cterm=reverse   ctermfg=none      ctermbg=none

if &term =~ "linux"
	hi TabLine       cterm=none ctermfg=white ctermbg=none
	hi TabLineSel    cterm=none ctermfg=white ctermbg=blue
	hi TabLineFill   cterm=none ctermfg=white ctermbg=none
	hi StatusLine    cterm=none ctermfg=black ctermbg=blue
	hi StatusLineNC  cterm=none ctermfg=white ctermbg=none
endif

if v:version >= 700
	au InsertEnter * hi StatusLine cterm=none ctermfg=white ctermbg=blue
	au InsertLeave * hi StatusLine cterm=none ctermfg=black ctermbg=blue
endif

hi SyntasticError   cterm=none      ctermfg=white ctermbg=blue
hi SyntasticWarning cterm=underline ctermfg=white ctermbg=none

" Taglist
hi TagListFileName cterm=none ctermfg=blue ctermbg=none

" ctrlp
hi CtrlPMatch cterm=underline ctermfg=white     ctermbg=none
hi CtrlPStats cterm=none ctermfg=black ctermbg=blue

" XML
hi link xmlEndTag Function

" Diff
hi diffIdentical cterm=none ctermfg=white ctermbg=none
hi diffAdded     cterm=none ctermfg=darkgreen ctermbg=none

" syntax
hi Comment     cterm=none ctermfg=darkgrey ctermbg=none
hi PreProc     cterm=none ctermfg=white    ctermbg=none
hi Constant    cterm=none ctermfg=blue  ctermbg=none
hi Type        cterm=none ctermfg=blue      ctermbg=none
hi Statement   cterm=bold ctermfg=white    ctermbg=none
hi Identifier  cterm=none ctermfg=blue      ctermbg=none
hi Ignore      cterm=none ctermfg=darkgray ctermbg=none
hi Special     cterm=none ctermfg=blue  ctermbg=none
hi Error       cterm=none ctermfg=white    ctermbg=blue
hi Todo        cterm=none ctermfg=white    ctermbg=blue
hi Underlined  cterm=none ctermfg=blue  ctermbg=none
hi Number      cterm=none ctermfg=blue  ctermbg=none
hi Function    cterm=none ctermfg=white    ctermbg=none
hi Define      cterm=bold ctermfg=white    ctermbg=none

hi link String          Constant
hi link Character       Constant
hi link Number          Constant
hi link Boolean         Constant
hi link Float           Number
hi link Number          Constant
hi link Repeat          Statement
hi link Label           Statement
hi link Keyword         Statement
hi link Exception       Statement
hi link Operator        Statement
hi link Include         PreProc
hi link Macro           PreProc
hi link PreCondit       PreProc
hi link StorageClass    Type
hi link Structure       Type
hi link Typedef         Type
hi link Tag             Special
hi link SpecialChar     Special
hi link Delimiter       Normal
hi link SpecialComment  Special
hi link Debug           Special
hi link Conditional     Statement
